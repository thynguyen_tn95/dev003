<?php


//default
$title = "sitename";
$description = "description";
$keyword = "key1,key2";


if($pageid == "index"){
	$title = "Very nice company";
	$description = "It is a very nice company website.";
	$keyword = "very, nice, company";
} elseif ($pageid == "company") {
	$title = "Company - Very nice company";
	$description = "It is a very nice company website \"Company\" page.";
	$keyword = "very, nice, company";
} elseif ($pageid == "service") {
	$title = "Service - Very nice company";
	$description = "It is a very nice company website \"Service\" page.";
	$keyword = "very, nice, company, service";
} elseif ($pageid == "news") {
	$title = "News - Very nice company";
	$description = "It is a very nice company website \"News\" page.";
	$keyword = "very, nice, company, news";
} elseif ($pageid == "contact") {
	$title = "Contact - Very nice company";
	$description = "It is a very nice company website \"Contact\" page.";
	$keyword = "very, nice, company, contact";
}


?>
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $description; ?>">
<meta name="keywords" content="<?php echo $keyword; ?>">